package br.com.senac.syschurrasco.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Date;

import br.com.senac.syschurrasco.R;
import br.com.senac.syschurrasco.model.Atendimento;
import br.com.senac.syschurrasco.model.Cliente;
import br.com.senac.syschurrasco.model.ItemAtendimento;
import br.com.senac.syschurrasco.model.Produto;

public class AtendimentoActivity extends AppCompatActivity {

    private Atendimento atendimento = new Atendimento();

    public AtendimentoActivity(){
        this.init();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atendimento);


    }


    public void init(){



        this.atendimento.setCliente(new Cliente("Daniel Santiago"));
        this.atendimento.setData(new Date());
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(1 , "Churraquinho1") ,2,5));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(1 , "Churraquinho2") ,2,5));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(1 , "Churraquinho3") ,2,5));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(1 , "Churraquinho4") ,2,5));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(1 , "Churraquinho5") ,2,5));
        this.atendimento.getItens().add(new ItemAtendimento(new Produto(1 , "Churraquinho6") ,2,5));

    }
}
