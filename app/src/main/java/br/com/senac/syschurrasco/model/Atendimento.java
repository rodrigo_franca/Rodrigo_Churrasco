package br.com.senac.syschurrasco.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sala304b on 05/09/2017.
 */

public class Atendimento {

    private Date data;
    private Cliente cliente;
    private List<ItemAtendimento> itens = new ArrayList<>();

    public Atendimento() {
    }

    public Atendimento(Date data, Cliente cliente, List<ItemAtendimento> itens) {
        this.data = data;
        this.cliente = cliente;
        this.itens = itens;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemAtendimento> getItens() {
        return itens;
    }

    public void setItens(List<ItemAtendimento> itens) {
        this.itens = itens;
    }
}
