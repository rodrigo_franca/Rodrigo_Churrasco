package br.com.senac.syschurrasco.model;

import java.util.Date;
import java.util.List;

/**
 * Created by sala304b on 05/09/2017.
 */

public class Cliente {

    private String nome;

    public Cliente() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}