package br.com.senac.syschurrasco.model;

/**
 * Created by sala304b on 05/09/2017.
 */

public class ItemAtendimento {

    private Produto produto;
    private int quantidade;
    private double preco;

    public ItemAtendimento() {
    }

    public ItemAtendimento(Produto produto, int quantidade, double preco) {
        this.produto = produto;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
