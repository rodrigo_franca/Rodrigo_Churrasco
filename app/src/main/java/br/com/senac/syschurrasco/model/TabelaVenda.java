package br.com.senac.syschurrasco.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sala304b on 05/09/2017.
 */

public class TabelaVenda {

    private Date vigenciaInicio;
    private Date getVigenciaFim;
    private List <ItemTabelaVenda> itens  = new ArrayList<>();

    public TabelaVenda() {

    }

    public TabelaVenda(Date vigenciaInicio, Date getVigenciaFim, List<ItemTabelaVenda> itens) {
        this.vigenciaInicio = vigenciaInicio;
        this.getVigenciaFim = getVigenciaFim;
        this.itens = itens;
    }

    public Date getVigenciaInicio() {
        return vigenciaInicio;
    }

    public void setVigenciaInicio(Date vigenciaInicio) {
        this.vigenciaInicio = vigenciaInicio;
    }

    public Date getGetVigenciaFim() {
        return getVigenciaFim;
    }

    public void setGetVigenciaFim(Date getVigenciaFim) {
        this.getVigenciaFim = getVigenciaFim;
    }

    public List<ItemTabelaVenda> getItens() {
        return itens;
    }

    public void setItens(List<ItemTabelaVenda> itens) {
        this.itens = itens;
    }

    public void add(ItemTabelaVenda item) {
        this.itens.add(item);
    }

    public void remove (ItemTabelaVenda item) {

        this.itens.remove(item);
    }
}
