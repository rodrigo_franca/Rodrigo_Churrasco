package br.com.senac.syschurrasco.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import br.com.senac.syschurrasco.R;
import br.com.senac.syschurrasco.model.ItemTabelaVenda;
import br.com.senac.syschurrasco.model.Produto;
import br.com.senac.syschurrasco.model.TabelaVenda;

public class ListaTabelaVendasActivity extends AppCompatActivity {

    private TabelaVenda tabelaVenda = new TabelaVenda();
    private ListView  listViewTabelaVendas ;

    public ListaTabelaVendasActivity(){
super();
        initTabelaVendas();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_tabela_vendas);


        int layout = android.R.layout.simple_list_item_multiple_choice;

        listViewTabelaVendas = (ListView) findViewById(R.id.listaTabelaVendas);

        ArrayAdapter<ItemTabelaVenda> adpter = new ArrayAdapter<ItemTabelaVenda>(this, layout, tabelaVenda.getItens());

        listViewTabelaVendas.setAdapter(adpter);

        //tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasco"), 5));

    }

    private void initTabelaVendas() {

        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasco de Alcatra"), 5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasco de Carne de Sol"), 3.5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Contra File"), 5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Contra File com Bacon"), 5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Cupim"), 5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Kafta de Carne"), 5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Medalão de Picanha"), 5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Meio de Asa"), 5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Frango"), 5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de Coração"), 5));
        tabelaVenda.add(new ItemTabelaVenda(new Produto(1, "Churrasquinho de "), 5));






    }
}
