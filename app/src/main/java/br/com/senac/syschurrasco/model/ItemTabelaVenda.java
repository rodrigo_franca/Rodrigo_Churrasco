package br.com.senac.syschurrasco.model;

import java.text.NumberFormat;

/**
 * Created by sala304b on 05/09/2017.
 */

public class ItemTabelaVenda {

    private Produto produto;
    private double preco;
   private NumberFormat nf = NumberFormat.getCurrencyInstance();

    public ItemTabelaVenda() {

    }

    public ItemTabelaVenda(Produto produto, double preco) {
        this.produto = produto;
        this.preco = preco;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    @Override
    public String toString() {
        return this.produto.getNome() + " - " + nf.format(this.getPreco()) ;
    }
}
